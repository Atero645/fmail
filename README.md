## FMail ##
![LGTM Grade](https://img.shields.io/lgtm/grade/python/bitbucket/Atero645/fmail)
![PyPI](https://img.shields.io/pypi/v/fmail.svg)
![PyPI - Python Version](https://img.shields.io/pypi/pyversions/fmail.svg)
![PyPI - License](https://img.shields.io/pypi/l/fmail)
[![Documentation Status](https://readthedocs.org/projects/fmail/badge/?version=latest)](https://fmail.readthedocs.io/en/latest/?badge=latest)

FMail is a small library build on top of smtplib.
Main goal is to allow send mails with attachments easily.

### TO DO ###

- Html mails
- Error handling
- Flask integration tests
- Celery integration
- Examples and docs
- tests


