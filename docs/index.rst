.. FMail documentation master file, created by
   sphinx-quickstart on Sun Oct 18 13:20:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FMail's documentation!
=================================

.. toctree::
   :maxdepth: 2
   
   installation
   quickstart

API Reference
===================
.. toctree::
   :maxdepth: 2

   api

