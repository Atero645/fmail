import pathmagic
import os
from fmail import FMailConfig, FMail, FMessage, FAttachment
from dotenv import load_dotenv

def test_send():
    load_dotenv(verbose=True)

    config_data = {
        'host': os.getenv("FMAIL_HOST"),
        'port': os.getenv("FMAIL_PORT"),
        "username": os.getenv("FMAIL_USERNAME"),
        'password': os.getenv("FMAIL_PASSWORD"),
    }

    config = FMailConfig(**config_data)
    fmail = FMail(config)
    attachment = FAttachment('tests/attachment.txt')

    html = """
    <html>
        <body>
            <h3>Hello,</h3>
            <p>this is <b style="color:red;">test</b> mail</p>
        </body
    </html>
    """

    msg = FMessage(**{
        'from_addr': os.getenv("FMAIL_USERNAME"),
        'to_addrs': [os.getenv("FMAIL_TO_TEST")],
        'subject': "FMail - test mail",
        'body_plain': "Hello this is a test",
        'body_html':html,
        'attachments': [attachment]
    })

    fmail.send(msg)
